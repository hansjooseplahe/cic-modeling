# Community_Inclusion_Currencies
Repository for Complex Systems model of the Grassroots Economics Community Inclusion Currency project. The Colab notebooks are able to be run and played with by anyone who uses the link. Modeling is built in [cadCAD](https://cadcad.org/). 

### Background & Context
In  developing  economies,  the  availability  of  national  currency  often  has  a  low correlation with the local capacity or demand. Instead of providing only aid based programs to help alleviate poverty, using markets to  help  alleviate  poverty  and  grow  economies  is  becoming  more  common. One of the issues with many aid based development programs is the flow of aid funds individuals in low liquidity areas right back to city centers and financiers, creating a never ending cycle of liquidity constraints. The goal of Community Inclusion  Currencies  and  other  market  based  approaches  to  reducing  poverty and  liquidity  constraints  in  poverty  stricken  areas  is  to  the  close  the  loop  of net cash outflows by providing an incentives program that keeps the liquidity in local economies. In this notebook we simulate the proposed Grassroots Economics Foundation's Community Inclusion Currency implementation deployed in Kenya using a graphbased dynamical system model in order to provide a scaffold for macroprudential economy planning.


## Simulations
For a walkthrough of the CIC model and what it can be used for, please watch this video: [https://www.youtube.com/watch?v=-tYqZ5_bnC8&t=0s](https://www.youtube.com/watch?v=-tYqZ5_bnC8&t=0s)

### Simulation work
This is the main notebook of simulation work for the CIC model. This mdoel can serve as a 'digital twin' to the live CIC system, and provide important operational decision support to key questions about future system settings & parameters.

[Click here to see the notebook of CIC simulations](https://nbviewer.jupyter.org/urls/gitlab.com/grassrootseconomics/cic-modeling/-/raw/master/Simulation/CIC_Network_cadCAD_model.ipynb)

### Initialization & Theory Work
BlockScience carried out a formalized derivation of the CIC bonding curve shape & mathematics, identifying explicit parameters and determining appropriate initialization conditions.

[Click here to see the notebook of bonding curve initialization testing](https://nbviewer.jupyter.org/urls/gitlab.com/grassrootseconomics/cic-modeling/-/raw/master/BondingCurve/cic_initialization.ipynb)

Conclusions: No red flags raised with initial system assumptions
- (P0) of 1 CIC = 1 KSH
- Target Reserve Ratio (TRR) of 25%
- Fees currently set to 0 for initial system setup.


### Subpopulation initialization 
For the purposes of modeling large amounts of agent data, in this notebook we "zoom up" from the agent level (micro-scale) to the subpopulation level (meso-scale), in order to optimize the amount of compute time in Monte Carlo analyses. 

[Click here to see the notebook of subpopulation initiatlization testing](https://nbviewer.jupyter.org/urls/gitlab.com/grassrootseconomics/cic-modeling/-/raw/master/SubpopulationGenerator/Subpopulation_Construction.ipynb)

### Parameter sweep 
The following notebook is a template of various parameter sweeps that can be carried out for specific parameter tests in the next phase of collaboration on this model. The results in this notebook are a demonstration only, and should not be taken as rigorous analysis of parameter choice.

[Click here to see the notebook template for parameter sweeps](https://nbviewer.jupyter.org/urls/gitlab.com/grassrootseconomics/cic-modeling/-/raw/master/Simulation_param/CIC_Network_cadCAD_model_params_Template.ipynb)


### Google Colab - last refresh and synchronisation date: 5-26-2020 - DEPRECIATED
Google colab provides interactive notebooks that can be run on Google cloud services, so you don't need to set up a local environment.
 
#### Note: Colabs use 10 instead of 50 subpopulation clusters for speed improvements in Monte Carlo simulations. 
[Click here to get to an interactive notebook](https://colab.research.google.com/drive/1JkpX6UwJAezxUkVVj2SHFah-eNUzEif0)

[Click here to for an interactive notebook with a parameter sweep](https://colab.research.google.com/drive/1_vtPeTrEEq95RlyHu9awSRMuXUgr0WAt)


## Role Taxonomy
Block Science produced a role taxonomy of the CIC project. 

[Click here to see the Role Taxonomy](https://gitlab.com/grassrootseconomics/cic-modeling/-/blob/master/Documents/2020.05.25_RedCrossCICRoleTaxonomy.pdf) to view it.


## Background information & concepts addressed

### What is cadCAD?
cadCAD (complex adaptive dynamics Computer-Aided Design) is a python based modeling framework for research, validation, and Computer Aided Design of complex systems. Given a model of a complex system, cadCAD can simulate the impact that a set of actions might have on it. This helps users make informed, rigorously tested decisions on how best to modify or interact with the system in order to achieve their goals. cadCAD supports different system modeling approaches and can be easily integrated with common empirical data science workflows. Monte Carlo methods, A/B testing and parameter sweeping features are natively supported and optimized for.

See [cadCAD on Github](https://github.com/BlockScience/cadCAD/tree/master/tutorials) for some tutorials on how to use cadCAD.
More cadCAD resources:
* https://community.cadcad.org/t/introduction-to-cadcad/15
* https://community.cadcad.org/t/putting-cadcad-in-context/19
* https://github.com/BlockScience/cadCAD/tree/master/tutorials

### Reproducibility
In order to reperform this code, we recommend the researcher use the following link to download https://www.anaconda.com/products/individual to download Python 3.7.To install the specific version of cadCAD this repository was built with, run the following code:
```pip install cadCAD==0.4.22```

Then run ```cd Community_Inclusion_Currencies ``` to enter the repository. Finally, run ```jupyter notebook``` to open a notebook server to run the various notebooks in this repository. 

#### Bonding Curves
* [From Curved Bonding to Configuration Spaces](https://epub.wu.ac.at/7385)

####  Systems Thinking
* https://community.cadcad.org/t/introduction-to-systems-thinking/18
* https://community.cadcad.org/t/working-glossary-of-systems-concepts/17


#### Token Engineering
* https://blog.oceanprotocol.com/towards-a-practice-of-token-engineering-b02feeeff7ca
* https://assets.pubpub.org/sy02t720/31581340240758.pdf

#### Community Currencies
* https://www.investopedia.com/terms/c/community_currencies.asp

#### Subpopulation modeling

#### Complex systems
* https://www.frontiersin.org/articles/10.3389/fams.2015.00007/full
* https://epub.wu.ac.at/7433/1/zargham_paruch_shorish.pdf

#### Network theory

#### Economics
* https://ergodicityeconomics.com/lecture-notes/

#### Systems Engineering
* http://systems.hitchins.net/systems-engineering/se-monographs/seessence.pdf


